require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user = users(:hoid)
    @other_user = users(:root)
  end

  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  test "should get signup" do
    get :new
    assert_response :success
    assert_select "title", "Sign up | Ruby on Rails Tutorial Sample App"    
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @user, user: {name: @user.name, email:@user.email}
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when logged in as the wrong user" do
    log_in_as(@user)
    patch :update, id: @other_user, user: {name: @other_user.name, email:@other_user.email}
    assert flash.empty?
    assert_redirected_to root_url
  end

  test 'should redirect destroy when not logged in' do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end

  test 'should redirect destroy when logged in as non-admin' do
    log_in_as(@user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @other_user
    end
  end

  test 'should destroy when logged in as admin' do
    log_in_as(@other_user)
    assert_difference 'User.count', -1 do
      delete :destroy, id: @user
    end
  end

  test 'should not allow the admin attribute to be edited via web' do
    log_in_as(@user)
    password = "passwordpassword"
    assert_not @user.admin?
    patch :update, id: @user, user: {password: password, password_confirmation: password, admin: true} 
    assert_not @user.reload.admin?
  end
end
