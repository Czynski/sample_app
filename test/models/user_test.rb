require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "John Madden", email: "football@youarejohnmadden.org", password:"devilbunnyneedsaham", password_confirmation:"devilbunnyneedsaham")
  end

  test 'should be valid' do
    assert @user.valid?
  end

  test 'should fail with empty name' do 
    @user.name=nil
    assert_not @user.valid?
    @user.name=""
    assert_not @user.valid?
    @user.name="        "
    assert_not @user.valid?
  end

  test 'should fail with empty email' do
    @user.email = nil
    assert_not @user.valid?
    @user.email = ""
    assert_not @user.valid?
    @user.email = "          "
    assert_not @user.valid?
  end

  test 'username and email should be within length limits' do
    @user.name = "a"*51
    assert_not @user.valid?
    @user.name = "John Madden"
    @user.email = "#{"a"*200}@#{"b"*51}.com"
    assert_not @user.valid?
  end

  test 'should accept correctly-formed email' do
    valid_addresses = %w[user@example.com foo@bar.org d@b.ca A_US-ER@foo.bar.org first.last@gmail.jp alice+bob@eve.cn]
    valid_addresses.each do |address|
      @user.email = address
      assert @user.valid? "#{address.inspect} should be valid"
    end
  end

  test 'should reject badly-formed email' do
    invalid_addresses = %w[user@example,com JohnMadden foo_at_bar.org d@b.ca.A_US-ER@foo.bar.org first.last@gmail_jp alice+bob@eve+cn foo@bar..com] 
    invalid_addresses.each do |address|
      @user.email = address
      assert_not @user.valid?, "#{address.inspect} should be invalid"
    end
  end

  test 'should not accept duplicate emails' do
    clone = @user.dup
    @user.save
    assert_not clone.valid?
    clone.name = "Morphling Changeling"
    assert_not clone.valid?, "user with same email and different name should remain invalid"
    clone.email = @user.email.upcase
    assert_not clone.valid?, "email duplication should be case-insensitive"
  end

  test 'email should be stored as lowercase' do
    mixed_case_address = "FOO@BaR.baZ"
    @user.email = mixed_case_address
    @user.save
    @user.reload
    assert_equal @user.email, mixed_case_address.downcase, "should be lowercase once saved"
  end

  test 'password should be fairly long but need not have special characters' do
    @user.password = @user.password_confirmation = 'a'*11
    assert_not @user.valid?
  end

  test 'to handle case of logout in other browers, the nil-digest user should authenticate to false' do
    assert_not @user.authenticated?(:remember,'')
  end

  #micropost tests

  test 'associated microposts are destroyed alongside user' do
    @user.save
    @user.microposts.create!(content: "Carthago delenda est.")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end
end
