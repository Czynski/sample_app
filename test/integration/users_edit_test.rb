require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:hoid)
  end
  
  test 'unsuccessful edit returns to edit screen' do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name:  "",
                                    email: "oo@foo",
                                    password: "foo",
                                    password_confirmation: "bar" }
    assert_template 'users/edit'
  end
  test "successful edit with friendly forwarding which clears out properly" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name = "Jim Fix"
    email = "jimfix@reed.edu"
    patch(user_path(@user), user: {name: name, email: email, password: "", password_confirmation: ""})
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.name, name
    assert_equal @user.email, email
    #Check that the session forwarder has been cleared
    assert_nil session[:forwarding_url] 
    delete logout_path
    log_in_as(@user)
    assert_redirected_to @user
    assert_nil session[:forwarding_url] 
  end
end
