require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid user signup does not enter database" do
    get signup_path
    assert_no_difference('User.count') do post(users_path, user: {name: "", email: "foo@broke", password: "baz", password_confirmation: "bar"}) end
    #evaluates the first argument 'User.count' before and after running the block and passes if equal
    assert_template('users/new')
  end
  test "valid user signup with activation enters database" do
    delete logout_path
    get signup_path
    assert_difference('User.count',1) do post users_path, user: {name: "James Buchanan",
                                                                 email:"buchanan@whitehouse.gov",
                                                                 password: "tippencanoeandtylertoo",
                                                                 password_confirmation:"tippencanoeandtylertoo"} end

    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    assert_not is_logged_in?
    #try an unactivated login
    log_in_as(user)
    assert_not is_logged_in?
    #valid token, wrong email
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    #valid activation token and email
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect!
    assert_template 'users/show'
    assert is_logged_in?
  end
end
