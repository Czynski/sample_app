require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = "Ruby on Rails Tutorial Sample App"
    @nonadmin = users(:hoid)
    @admin = users(:root)
  end

  test "layout links" do
    delete logout_path
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count:2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    
    log_in_as(@nonadmin)
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count:2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
 
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", edit_user_path(@nonadmin)
    assert_select "a[href=?]", user_path(@nonadmin)

  end

  test "should get signup" do
    get signup_path
    assert_response :success
    assert_select "title", full_title("Sign up")
  end
end
