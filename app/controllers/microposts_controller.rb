class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: :destroy

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      #@feed_items = Micropost.all.paginate(page: params[:page])
      @feed_items = []
      #redirect_to request.referrer || root_url
      render 'static_pages/home'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to request.referrer || root_url
  end

  private
    def micropost_params
      params.require(:micropost).permit(:content)
    end

    def correct_user
      if !current_user.admin?
        @micropost = current_user.microposts.find_by(id: params[:id])
      else
        @micropost = Micropost.all.find_by(id: params[:id])
      end
      redirect_to root_url if @micropost.nil?
    end
end
