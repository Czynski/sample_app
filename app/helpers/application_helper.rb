module ApplicationHelper

  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end
  def message_tag(type, text, tag)
	  "<#{tag} class =\"alert alert-#{type}\">#{text}</#{tag}>".html_safe
    #this duplicates a limited case of rails function 'content-tag' and is not particularly useful.
    #it is included only as a demonstration of my solution to the loose prompt:
    #'this code is ugly; cleaning it up is left as an exercise'
    #The synonymous code is:
    #content_tag(tag, text, class: "alert alert-#{type}")
    #first attempt ommitted 'html_safe" but this was quickly corrected
  end
end
